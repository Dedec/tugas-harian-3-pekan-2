<?php

require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$sheep = new Animal("shaun");

echo "Name: " . $sheep->name . "<br>";
echo "Legs: " . $sheep->legs . "<br>";
echo "Cold Blooded: " . $sheep->cold_blooded . "<br> <br>";

$toad = new Frog("Buduk");

echo "Name: " . $toad->name . "<br>";
echo "Legs: " . $toad->legs . "<br>";
echo "Cold Blooded: " . $toad->cold_blooded . "<br>";
echo $toad->jump();
echo "<br> <br>";


$monkey = new Ape("Kera Sakti");
echo "Name: " . $monkey->name . "<br>";
echo "Legs: " . $monkey->legs . "<br>";
echo "Cold Blooded: " . $monkey->cold_blooded . "<br>";
echo $monkey->yell();
